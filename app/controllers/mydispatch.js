var mydispatchApp = angular.module('mydispatchApp', ['MydispatchModel', 'ArticleModel', 'CategoryModel', 'hmTouchevents']);


 // show = configuration select
 // index = list of articles from sections

// I'm not sure this is used anymore
// Index: http://localhost/views/mydispatch/index.html

mydispatchApp.controller('IndexCtrl', function ($scope, myDispatch, ADArticles) {

  // Helper function for opening new webviews
  $scope.open = function(id) {
    webView = new steroids.views.WebView("/views/article/show.html?id="+id);
    steroids.layers.push(webView);
  };

  myDispatch.getSections(function(initResults) {
    // set name
    // $scope.sectionName = window.localStorage.getItem("section");
    // loop through each results
    // we need to see if the view can take multiple sections, because AD wants to sort by date, not by section
    ADArticles.getByCategories(initResults).then(function(results) {
      var i = 0;
      var l = results.length;
      $scope.articles = [];
      var seenPhoto = false;
      for (; i < l; i++) {
        (function(i) {
          // look up the articles in the DB
          ADArticles.getArticle(results[i].nid, function(data) {
            // first article with logic
            if (data.photo != null && !seenPhoto) {
              $scope.firstArticle = data;
              seenPhoto = true;
            }
            else {
              $scope.articles.push(data);
            }
            $scope.$apply();
          });
        })(i);
      }
    })
  });
});


// Show: http://localhost/views/mydispatch/show.html?id=<id>
// comes from ios drawer
mydispatchApp.controller('ShowCtrl', function ($scope, $parse, CategoryRestangular, myDispatch, ADArticles) {


  var homeScreen = new steroids.views.WebView({
    location: "/index.html",
    id: 'homescreenView'
  });

  var homeButton = new steroids.buttons.NavigationBarButton();
  homeButton.imagePath = "/images/home-button@2x.png";
  homeButton.onTap = function() {
    drawer = false;
    steroids.layers.popAll();
    steroids.layers.push(homeScreen);
  };
 
  steroids.view.navigationBar.update({
    buttons: {
      right: [homeButton],
    }
  });

  var mydispatch = false;

  myDispatch.getDispatchSetting(function(results) {

    mydispatch = (typeof results === 'undefined' || results.setting === 0) ? false : true;
    $scope.mydispatch = mydispatch;
    $scope.$apply();
  
  });

  // toggled is called whenever a section is tapped - could be on, or could be off
  $scope.toggleStatus = function(id) {
    myDispatch.getItem(id, function(results) {
      if (typeof results === "undefined") {
        myDispatch.saveData(id, function(data) {
          // it's been saved
        });
      } // end if
      else {
        myDispatch.removeItem(id, function(data) {
          // item removed
        });
      } // end else
    });
  }

  $scope.toggleMyDispatch = function() {
    myDispatch.getDispatchSetting(function(results) {
      if (typeof results === "undefined" || results.setting === 0) {
        myDispatch.saveDispatchSetting(function(data) {
          var msg = { reload: 'homescreen' };
          window.postMessage(msg, "*");
        });
      } // end if
      else {
        myDispatch.removeDispatchSetting(function(data) {
          var msg = { reload: 'homescreen' };
          window.postMessage(msg, "*");
        });
      } // end else
    });
  }

  var seencategories = [];
  $scope.categories = [];
  CategoryRestangular.all('categories').getList().then(function(categories) {
    var i = 0;
    var l = categories.length;
    var dynamic_name;
    var dynamic_model;
    var category_value;
    for(; i < l; i++) {
      (function(i) {        
        CategoryRestangular.all(categories[i].name).getList().then(function(sub) {

          var i2 = 0;
          var l2 = sub.length;
          for(; i2 < l2; i2++) {
            if (seencategories.indexOf(sub[i2].tid) == -1) {
              
              // checked
              (function(i2) {
                myDispatch.getItem(sub[i2].tid, function(results) {
                  
                  category_value = (typeof results === 'undefined') ? false : true;
                  sub[i2].classname = category_value;

                  
                  $scope.categories.push(sub[i2]);
                  $scope.$apply();
                });
              })(i2);
              seencategories.push(sub[i2].tid);
            }  // end if
          } // end for
          
        });
      })(i);        
    }
  });
  

});


mydispatchApp.controller('SettingsCtrl', function ($scope, $parse, CategoryRestangular, myDispatch, ADArticles) {

 var homeScreen = new steroids.views.WebView({
    location: "/index.html",
    id: 'homescreenView'
  });

  var homeButton = new steroids.buttons.NavigationBarButton();
  homeButton.imagePath = "/images/home-button@2x.png";
  homeButton.onTap = function() {
    drawer = false;
    steroids.layers.popAll();
    steroids.layers.push(homeScreen);
  };
 
  steroids.view.navigationBar.update({
    buttons: {
      right: [homeButton],
    }
  });

  $scope.toggleMyDispatch = function() {
    myDispatch.getDispatchSetting(function(results) {
      if (typeof results === "undefined" || results.setting === 0) {
        myDispatch.saveDispatchSetting(function(data) {
          var msg = { reload: 'homescreen' };
          window.postMessage(msg, "*");
        });
      } // end if
      else {
        myDispatch.removeDispatchSetting(function(data) {
          var msg = { reload: 'homescreen' };
          window.postMessage(msg, "*");
        });
      } // end else
    });
  }

  $scope.toggleLargeFont = function() {
    ADArticles.getSectionFontSize(function(result) {
      // font sizes should be moved to a global conf
      size = (result == 1.25) ? .85 : 1.25;
      ADArticles.saveSectionFont(size, function(saveresults) {
        var msg = { reload: 'homescreen' };
        window.postMessage(msg, "*");
        var msg = { reload: 'sections' };
        window.postMessage(msg, "*");
      });
    })
  }

  var mydispatch = false;
  var largefont = false;

  myDispatch.getDispatchSetting(function(results) {

    mydispatch = (typeof results === 'undefined' || results.setting === 0) ? false : true;
    $scope.mydispatch = mydispatch;
    $scope.$apply();
  
  });

  ADArticles.getSectionFontSize(function(result) {
    $scope.largefont = (result == 1.25) ? true : false;
    $scope.$apply();
  });

});