var savedstoryApp = angular.module('savedstoryApp', ['SavedstoryModel', 'ArticleModel', 'hmTouchevents']);


// Index: http://localhost/views/savedstory/index.html

savedstoryApp.controller('IndexCtrl', function ($scope, ADArticles, savedStories) {

  // Helper function for opening new webviews
  $scope.open = function(id) {
    webView = new steroids.views.WebView("/views/article/show.html?id="+id);
    steroids.layers.push(webView);
  };

  $scope.remove = function(item) {
    savedStories.removeItem(item.id, function(result) {
      //alert("Removed \"" + item.headline + "\"")
      // toggle item show state?
      // @todo: indicate saved story state
      
    });
    item.hide = true;
    // how to refresh
    //webView = new steroids.views.WebView("/views/article/show.html?id="+id);
    //steroids.layers.push(webView);
  };

  savedStories.getStories(function(results) {
    var i = 0;
    var l = results.length;
    $scope.articles = [];
    for(; i < l; i++) {
      (function(i) {
        ADArticles.getArticle(results[i].nid, function(data) {
          data.hide = false;
          $scope.articles.push(data);
          $scope.$apply();
        });
      })(i);
    }
  });

  

});


/*
// Show: http://localhost/views/savedstory/show.html?id=<id>
savedstoryApp.controller('ShowCtrl', function ($scope, $filter, SavedstoryRestangular) {

  // Fetch all objects from the local JSON (see app/models/savedstory.js)
  SavedstoryRestangular.all('savedstory').getList().then( function(savedstorys) {
    // Then select the one based on the view's id query parameter
    $scope.savedstory = $filter('filter')(savedstorys, {savedstory_id: steroids.view.params['id']})[0];
  });

  // -- Native navigation
  steroids.view.navigationBar.show("Savedstory: " + steroids.view.params.id );

});
*/