var articleApp = angular.module('articleApp', ['ArticleModel', 'SavedstoryModel', 'MydispatchModel', 'hmTouchevents']);


// homescreen
articleApp.controller('IndexCtrl', function ($scope, ADArticles, myDispatch) {
  
  // sets loading gif
  // appgyver doesn't support keepLoading on homescreen yet 
  $scope.homeloading = true;
  $scope.loading = '<div class="centered"><h1>Loading...</h1><img id="spinner" src="vendor/topcoat/img/spinner2x.png"></div>';

  // Helper function for opening new webviews
  $scope.open = function(id) {    
    if (!drawer) {
      webView = new steroids.views.WebView('/views/article/show.html?id=' + id);
      steroids.layers.push(webView);
    };
  };
  
  loadHomeScreen(function() { });

  // listen for refresh from settings google
  window.addEventListener("message", function(msg) {
    if (msg.data.reload == 'homescreen') {
      loadHomeScreen(function() { });
    }
  });

  // refresh
  $('.refreshable').pullToRefresh({
      callback: function () {
         var deferred = $.Deferred();
         loadHomeScreen(function() {
            deferred.resolve();
         });
         return deferred.promise();
      }
  });



  // this is a simplified caller for data views
  // @todo: next release
  /*
  var urls = [
    'http://alaskadisstg.prod.acquia-sites.com/appdata/views/ad_app_feed?display_id=fpfs',
    'http://alaskadisstg.prod.acquia-sites.com/appdata/views/ad_app_feed?display_id=fpcn',
    'http://alaskadisstg.prod.acquia-sites.com/appdata/views/ad_app_feed?display_id=fpmq'
  ];

  ADArticles.getRESTData(urls).then(function(results) {
    $scope.articles = [];
    $scope.firstArticle = results.first;
    $scope.articles = results.articles;
    $scope.$apply();
  });
*/
  
  function loadHomeScreen(callback) {
    var mydispatch = false;

    ADArticles.getSectionFontSize(function(result) {
      $scope.size = function() {
        return { 
          'font-size': result + 'em' 
        };
      }
      $scope.$apply();
    })
    
    myDispatch.getDispatchSetting(function(results) {
      if (typeof results === "undefined" || results.setting === 0) {
        $scope.myDispatch = false;
        $scope.$apply();
        ADArticles.getHomeScreen().then(function(results) {
          processArticles(results);
        });      
      }
      else {
        $scope.myDispatch = true;
        $scope.$apply();
        myDispatch.getSections(function(sections) {
          ADArticles.getByCategories(sections).then(function(articles) {
            processArticles(articles);
          });
        });
      }
      callback();
    });
  

  }
  

  function processArticles(results) {
    var i = 0;
    var l = results.length;
    $scope.articles = [];
    var seenPhoto = false;
    for (; i < l; i++) {
      (function(i) {
        ADArticles.getArticle(results[i].nid, function(results) {
          if (results.photo != null && !seenPhoto) {
            $scope.firstArticle = results;
            seenPhoto = true;
          }
          else {
            $scope.articles.push(results);
          }         
          $scope.homeloading = false;
          $scope.$apply();
        });
      })(i);
    }
  }
    
});


// Show: http://localhost/views/article/show.html?id=<id>

articleApp.controller('ShowCtrl', function ($scope, $filter, ADArticles, savedStories) {

  ADArticles.applyNavButtons();  

  ADArticles.getArticle(steroids.view.params['id'], function(results) {

    // we need to apply href="openInBrowser(link)" to all hyperlinks in results.body

    $scope.article = results;
    $scope.$apply();
  });

  // duplicate this into sections
  ADArticles.getFontSize(function(result) {
    $scope.size = function() {
      return { 
        'font-size': result + 'em' 
      };
    }
    $scope.$apply();
  })


  $scope.adFavorite = function(item) {
    var headline = $("<div/>").html(item.headline).text();
    var message = "Article: \"" + headline + "\" saved ";
    //alert(");
    navigator.notification.alert(message, {}, "AlaskaDispatch");
    savedStories.saveData(item.id, function(result) {
      // @todo: indicate saved story state
    });
    
  }

  $scope.adChangeFont = function() {
    ADArticles.changeFont(function(result) {
      $scope.size = function() {
        return { 
          'font-size': result + 'em' 
        };
      }
      $scope.$apply();
    });
    
  }
  
  $scope.articleShare = false;
  // modal share window
  $scope.adShareWindow = function(item) {
    item = !item;
  }

  $scope.adOpenSafari = function(item) {
    var link = 'http://www.alaskadispatch.com/node/' + item.id;
    steroids.openURL(link);
  }

  $scope.openInBrowser = function(url) {
    steroids.openURL(url);
  }

  $scope.shareFacebook = function(article) {
    steroids.addons.facebook.ready.then(function() {
      steroids.addons.facebook.login({scope: 'email'}).then(function() {
        steroids.addons.facebook.dialog('feed', {
          link: 'http://www.alaskadispatch.com/node/' + article.id,
          caption: 'AlaskaDispatch: ' + article.headline
        }).then(function() {
          
        });
      }).error(function(e) {
        navigator.notification.alert("Something went wrong with the login. Error message: " + e.message, {}, "AlaskaDispatch");
      });
    });
    
  }

});

