var categoryApp = angular.module('categoryApp', ['CategoryModel', 'ArticleModel', 'hmTouchevents']);


// Index: http://localhost/views/category/index.html

categoryApp.controller('CategoriesCtrl', function ($scope, CategoryRestangular) {
  
  // Helper function for opening new webviews
  $scope.open = function(id) {
    // we could create a sqlite db of id and name based on feed, but that's a bit much
    webView = new steroids.views.WebView("/views/category/show.html?id="+id);
    steroids.layers.push( {
      view: webView, 
      keepLoading: true
    });
    //steroids.drawers.hideAll();
  };

  $scope.openFromDrawer = function(id, section, parent) {
    $scope.toggle(parent);
    window.localStorage.setItem("section", section);
    $scope.$apply();
    drawer = false;
    var msg = { drawer: drawer };
    window.postMessage(msg, "*");
    steroids.drawers.hideAll();
    $scope.open(id);
  };

  $scope.gotoSection = function(selection) {
    var url = '';
    var keeploading = false;
    switch(selection) {
      case 'mydispatch':
        url = '/views/mydispatch/show.html';
        break;
      case 'saved':
        url = '/views/savedstory/index.html';
        break;
      case 'most_read':
        url = '/views/category/mostread.html';
        keeploading = true;
        break;
      case 'submissions':
        url = '/share.html';
        break;
      case 'settings':
        url = '/settings.html';
        break;
      default: 
        url = '/views/mydispatch/show.html';
        break;
    }
    drawer = false;
    webView = new steroids.views.WebView(url);
    var msg = { drawer: drawer };
    window.postMessage(msg, "*");
    steroids.drawers.hideAll();
    steroids.layers.push({
      view: webView, 
      keepLoading: keeploading
    });
    
    /*
    webView.preload({},{
      onSuccess: function() {
        steroids.drawers.hideAll();
        //steroids.layers.replace(webView);
        steroids.layers.push(webView);
      }
    });
    */
  }



  // Fetch all objects from the local JSON (see app/models/category.js)
  $scope.categories = [];
  CategoryRestangular.all('categories').getList().then(function(categories) {
    var i = 0;
    var l = categories.length;
    for(; i < l; i++) {
      (function(i) {        
        CategoryRestangular.all(categories[i].name).getList().then(function(sub) {
          var tmp = {
            parent: categories[i].show,
            child: sub,
            show: false
          }          
          $scope.categories.push(tmp);
          $scope.$apply();  
        });
      })(i);
    }
  });

  $scope.toggle = function (item) {
    item.show = !item.show;
  }

});

categoryApp.controller('ShowCtrl', function ($scope, $filter, $q, ADArticles, CategoryRestangular) {

  $scope.open = function(id) {
    if (!drawer) {
      webView = new steroids.views.WebView("/views/article/show.html?id="+id);
      steroids.layers.push(webView);
    }
  };

  
  

  var tid = steroids.view.params['id'];  
  ADArticles.applyNavButtons();  
  $scope.sectionName = window.localStorage.getItem("section");

  window.addEventListener("message", function(msg) {
    if (msg.data.reload == 'sections') {
      loadSectionScreen(function() { });
    }
  });

  loadSectionScreen(function() { });


  function loadSectionScreen(callback) {

    ADArticles.getSectionFontSize(function(result) {
      $scope.size = function() {
        return { 
          'font-size': result + 'em' 
        };
      }
      $scope.$apply();
    })

    ADArticles.getByCategory(tid).then(function(results) {
      var i = 0;
      var l = results.length;
      $scope.articles = [];
      var seenPhoto = false;
      for (; i < l; i++) {
        (function(i) {
          ADArticles.getArticle(results[i].nid, function(data) {
            if (data.thumb != null && !seenPhoto) {
              $scope.firstArticle = data;
              seenPhoto = true;
            }
            else {
              $scope.articles.push(data);
            }          
            $scope.$apply();
          });
        })(i);
      }
    });
    callback();
  }
  
});


// this could be moved to categorycontroller above and pass in a param to switch the model 
categoryApp.controller('MostReadCtrl', function ($scope, ADArticles) {

  ADArticles.applyNavButtons();

  $scope.open = function(id) {
    if (!drawer) {
      webView = new steroids.views.WebView("/views/article/show.html?id="+id);
      steroids.layers.push(webView);
    }
  };
  
  ADArticles.getMostRead().then(function(results) {
    var i = 0;
    var l = results.length;
    $scope.articles = [];
    var seenPhoto = false;
    for (; i < l; i++) {
      (function(i) {
        ADArticles.getArticle(results[i].nid, function(data) {
          if (data.thumb != null && !seenPhoto) {
            $scope.firstArticle = data;
            seenPhoto = true;
          }
          else {
            $scope.articles.push(data);
          }          
          $scope.$apply();
        });
      })(i);
    }
  });
  
});