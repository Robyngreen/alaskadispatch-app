// The contents of individual model .js files will be concatenated into dist/models.js

(function() {

// Protects views where angular is not loaded from errors
if ( typeof angular == 'undefined' ) {
	return;
};


var module = angular.module('SavedstoryModel', []);

module.factory('savedStories', function() {
  var db = window.openDatabase("Database", "1.0", "userprefs", -1);
  return {
    getStories: function (callback) {
      var self = this;
      var stories = [];
      db.transaction(function(tx) {
        tx.executeSql("SELECT * FROM savedstories", [], function(tx, results) {
          var i = 0;
          var l = results.rows.length;
          for (; i < l; i++) {
            stories.push(results.rows.item(i));
          }
          callback(stories);
        }, self.errorCB);
      }, self.errorCB, self.successCB);

    },
    saveData: function (id, callback) {
      var self = this;
      db.transaction(
      function(tx) {
        tx.executeSql('INSERT OR IGNORE INTO savedstories (nid) VALUES (?)', [id], function(tx, results) {
      }, self.errorCB);
        callback(id);
      }, self.errorCB, self.successCB);
    },
    removeItem: function (id, callback) {
      var self = this;
      db.transaction(
      function(tx) {
        tx.executeSql('DELETE FROM savedstories WHERE nid=?', [id], function(tx, results) {
      }, self.errorCB);
        callback(id);
      }, self.errorCB, self.successCB);
    },
    errorCB: function (err) {
      //alert("Error processing SQL: "+err.code);
    },
    successCB: function () {
      //alert("success!");
    }

  }

});


})();
