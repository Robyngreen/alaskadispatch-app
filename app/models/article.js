// The contents of individual model .js files will be concatenated into dist/models.js

(function() {

  // Protects views where angular is not loaded from errors
  if ( typeof angular == 'undefined' ) {
    return;
  };

  var module = angular.module('ArticleModel', []);
  
  module.factory('ADArticles', ['$http', '$q', '$rootScope', function($http, $q, $rootScope) {
    delete $http.defaults.headers.common['X-Requested-With'];
    $http.defaults.useXDomain = true;
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    var db = window.openDatabase("Database", "1.0", "Articles", 5000000);
    var udb = window.openDatabase("Database", "1.0", "userprefs", -1);
    return {      
        getHomeScreen: function () {
            var self = this;
            var deferred = $q.defer();
            return $q.all([
                $http.get('http://www.alaskadispatch.com/appdata/views/ad_app_feed?display_id=fpfs'),
                $http.get('http://www.alaskadispatch.com/appdata/views/ad_app_feed?display_id=fpcn'),
                $http.get('http://www.alaskadispatch.com/appdata/views/ad_app_feed?display_id=fpmq')
            ]).then(function (results) {
                var aggregatedData = [];
                var counter = 0;
                angular.forEach(results, function (result) {
                    aggregatedData = aggregatedData.concat(result.data);
                });
                aggregatedData = self.exclude(aggregatedData);
                self.saveDB(aggregatedData, function(results) {
                  deferred.resolve(aggregatedData);
                  //steroids.view.removeLoading();
                  $rootScope.$apply();
                });

                // it'd be really awesome if this could return a DB object instead of the $http so that getArticle in controller isn't needed - less calls on homescreen
                // db objects aren't in the right order, would have to write an order save DB 
                return deferred.promise;

            });
        },
        getByCategory: function (id) {
          var self = this;
          var deferred = $q.defer();
          return $q.all([
            $http.get('http://www.alaskadispatch.com/appdata/views/ad_app_feed?args[0]=' + id + '&display_id=section_front'),
            /* we don't need this anymore because we moved the view above to pull taxonomy alongside the nodequeue, like the section front view
              $http({
              method: 'POST',
              url: 'http://www.alaskadispatch.com/appdata/taxonomy_term/selectNodes',
              data: 'tid=' + id + '&limit=10&' + encodeURIComponent('order[t.created]') + '=desc'
            }),*/
          ]).then(function (results) {
            var aggregatedData = [];
            angular.forEach(results, function (result) {
              aggregatedData = aggregatedData.concat(result.data);
            });
            aggregatedData = self.exclude(aggregatedData);
            self.saveDB(aggregatedData, function(results) {
              deferred.resolve(aggregatedData);
              steroids.view.removeLoading();
              $rootScope.$apply();

            });

            // it'd be really awesome if this could return a DB object instead of the $http so that getArticle in controller isn't needed - less calls on categoryscreen
            return deferred.promise;
          });
        },
        getByCategories: function (ids) {
          var self = this;
          var deferred = $q.defer();
          var new_ids = ids.join("+");
          return $q.all([
            $http.get('http://www.alaskadispatch.com/appdata/views/ad_app_feed?args[0]=' + new_ids + '&display_id=mydispatch'),
          ]).then(function (results) {
            var aggregatedData = [];
            angular.forEach(results, function (result) {
              aggregatedData = aggregatedData.concat(result.data);
            });
            aggregatedData = self.exclude(aggregatedData);
            self.saveDB(aggregatedData, function(results) {
              deferred.resolve(aggregatedData);
              steroids.view.removeLoading();
              $rootScope.$apply();
            });
            // it'd be really awesome if this could return a DB object instead of the $http so that getArticle in controller isn't needed - less calls on categoryscreen
            return deferred.promise;
          });
        },
        getMostRead: function (ids) {
          var self = this;
          var deferred = $q.defer();
          return $q.all([
            $http.get('http://www.alaskadispatch.com/appdata/views/ad_app_feed?display_id=most_read'),
          ]).then(function (results) {
            var aggregatedData = [];
            angular.forEach(results, function (result) {
              aggregatedData = aggregatedData.concat(result.data);
            });
            aggregatedData = self.exclude(aggregatedData);
            self.saveDB(aggregatedData, function(results) {
              deferred.resolve(aggregatedData);
              steroids.view.removeLoading();
              $rootScope.$apply();
            });
            // it'd be really awesome if this could return a DB object instead of the $http so that getArticle in controller isn't needed - less calls on categoryscreen
            return deferred.promise;
          });
        },
        exclude: function(items) {
          var new_items = [];
          var i = 0;
          var l = items.length;
          for(; i < l; i++) {
            if (items[i].type != 'op_image') {
              new_items.push(items[i]);
            }            
          }
          return new_items;
        },
        getNode: function (nid) {
          return $http({ url: 'http://www.alaskadispatch.com/appdata/node/' + nid, method: 'GET' });
        },
        getTerm: function (tid) {
          return $http({ url: 'http://www.alaskadispatch.com/appdata/taxonomy_term/' + tid, method: 'GET' });
        },
        getArticle: function (nid, callback) {
          var self = this;
          var article = {};

          db.transaction(function(tx) {
            tx.executeSql("SELECT * FROM articles WHERE id = ?", [nid], function(tx, results) {              
              if (results.rows.length !== 0) {
                callback(results.rows.item(0));
              }
              else {
                callback(article);
              }
            }, function(err) {
              callback(article);
            });
          }, self.errorCB, self.successCB);

        },
        saveDB: function (items, callback) {
          var articles = [];
          var i = 0;
          var l = items.length;
          var numCalls = 0;
          for(i = 0; i < l; i++) {
            
            var article = {};

            article.nid = items[i].nid;
            article.headline = items[i].node_title;
            article.body = items[i].body;
            article.date = items[i].posted;
            if (items[i].teaser != null) {        
              article.teaser = items[i].teaser;
            } // end if
            // we need a way to 'force' a teaser, because sometimes its not there, and we can't use .body in section digests.
            // the view that produces the REST does this.
            if (items[i].author != null) {        
              article.author = items[i].author;
            } // end if
            if (items[i].topics != null) {        
              article.section = items[i].topics;
            } // end if
            if (items[i].main_image != null) {
              var text = items[i].main_image;
              text = String(text).replace(/<img src=\"(.+?)\".*?>/gm, '$1');
              //<img src=\"http://www.alaskadispatch.com/sites/default/files/styles/ad_slideshow_400/public/november%20cold%20weather%2014.jpg?itok=7mwkh7wL\" width=\"400\" height=\"267\" alt=\"\" />","thumb":"
              article.photo = text;
            } // end if
            if (items[i].photocaption != null) {        
              article.photocaption = items[i].photocaption;
            } // end if
            if (items[i].photographer != null) {        
              article.photographer = items[i].photographer;
            } // end if
            if (items[i].thumb != null) {
              var text = items[i].thumb;
              text = String(text).replace(/<img src=\"(.+?)\".*?>/gm, '$1');
              //<img src=\"http://www.alaskadispatch.com/sites/default/files/styles/ad_slideshow_400/public/november%20cold%20weather%2014.jpg?itok=7mwkh7wL\" width=\"400\" height=\"267\" alt=\"\" />"                
              article.thumb = text;
            } // end if

            this.saveData(article, function(data) {
              articles.push(article);
              numCalls++;
              if (numCalls == l) {
                callback(articles);
              }
            });
          } // end for


          

        },
        saveData: function (article, callback) {
          var self = this;
          db.transaction(
          function(tx) {
            tx.executeSql('INSERT OR IGNORE INTO articles (id, headline, photo, author, body, section, date, teaser, photographer, photocaption, thumb) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [article.nid, article.headline, article.photo, article.author, article.body, article.section, article.date, article.teaser, article.photographer, article.photocaption, article.thumb], function(tx, results) {
          }, self.errorCB);
            callback(article);
          }, self.errorCB, self.successCB);
        },        
        errorCB: function (err) {
          //alert("Error processing SQL: "+err.code);
        },
        successCB: function () {
              //alert("success!");
        },
        changeFont: function(callback) {
          var self = this;
          self.getFontSize(function(result) {
            // font sizes should be moved to a global conf
            size = (result == 1.35) ? 1 : 1.35;
            self.saveFont(size, function(saveresults) {
              callback(size);
            });
          })
        },
        getFontSize: function(callback) {
          // font sizes should be moved to a global conf
          var size = 1;
          udb.transaction(function(tx) {
            tx.executeSql("SELECT setting FROM settings WHERE name = 'font-size'", [], function(tx, results) {              
              var len = results.rows.length;
              if (len > 0) {
                size = results.rows.item(0).setting;
              }
              callback(size);
            }, function(err) {
              callback(size);
            });
          });
        },          
        saveFont: function (size, callback) {
          var self = this;
          udb.transaction(
          function(tx) {
            var name = 'font-size';
            tx.executeSql('INSERT OR REPLACE INTO settings (name, setting) VALUES (?,?)', [name, size], function(tx, results) {
              callback(results);
            });        
          });
        },
        getSectionFontSize: function(callback) {
          // font sizes should be moved to a global conf
          var size = 0.85;
          udb.transaction(function(tx) {
            tx.executeSql("SELECT setting FROM settings WHERE name = 'section-font-size'", [], function(tx, results) {              
              var len = results.rows.length;
              if (len > 0) {
                size = results.rows.item(0).setting;
              }
              callback(size);
            }, function(err) {
              callback(size);
            });
          });
        },          
        saveSectionFont: function (size, callback) {
          var self = this;
          udb.transaction(
          function(tx) {
            var name = 'section-font-size';
            tx.executeSql('INSERT OR REPLACE INTO settings (name, setting) VALUES (?,?)', [name, size], function(tx, results) {
              callback(results);
            });        
          });
        },
        applyNavButtons: function() {

          var leftDrawer = new steroids.views.WebView({
            location: "/views/drawer/show.html",
            id: 'drawerView'
          });
          var homeScreen = new steroids.views.WebView({
            location: "/index.html",
            id: 'homescreenView'
          });

          var menuButton = new steroids.buttons.NavigationBarButton();
          var homeButton = new steroids.buttons.NavigationBarButton();
          menuButton.imagePath = "/images/menu-button@2x.png";
          menuButton.onTap = function() { 
            drawer = !drawer;
            steroids.drawers.hide();
            steroids.drawers.show(leftDrawer);
          };
          homeButton.imagePath = "/images/home-button@2x.png";
          homeButton.onTap = function() {
            drawer = false;
            steroids.layers.popAll();
            steroids.layers.push(homeScreen);
          };
         
          steroids.view.navigationBar.update({
            overrideBackButton: true,
            buttons: {
              left: [menuButton],
              right: [homeButton],
            }
          });

        }
    }

  }]);

  module.filter('htmlToPlaintext', function() {
    return function(text) {
      text = String(text).replace(/<(?:.|\n)*?>/gm, '');
      text = String(text).replace(/&nbsp;/gm, ' ');
      // ckeditor uses this syntax for inline files. We need to translate these fids to urls for display, but for now we'll remove
      //text = String(text).replace(/[[.*?]]/gm, '');
      return text;
    }
  });

  module.filter('removeCKEditor', function() {
    return function(text) {
      // ckeditor uses this syntax for inline files. We need to translate these fids to urls for display, but for now we'll remove
      text = String(text).replace(/\[\[.*?\]\]/gm, '');
      return text;
    }
  });

  module.directive( 'convertLinks', function ( $compile ) {
    return {
      scope: true,
      link: function ( scope, element, attrs ) {
        var compileText;
        attrs.$observe( 'data', function ( text ) {
          if ( angular.isDefined( text ) ) {
            // compile the provided template against the current scope
            text = String(text).replace(/href=("|')(.*?)("|')/gm, 'href="#" ng-click="openInBrowser(\'$2\')"');
            compileText = $compile( text )( scope );
            element.html(""); // dummy "clear"
            element.append( compileText );
          }
        });
      }
    };
  });

})();
