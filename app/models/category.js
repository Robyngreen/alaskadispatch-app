// The contents of individual model .js files will be concatenated into dist/models.js

(function() {

    // Protects views where angular is not loaded from errors
    if ( typeof angular == 'undefined' ) {
    	return;
    };


    var module = angular.module('CategoryModel', ['restangular']);

    module.factory('CategoryRestangular', function(Restangular) {

      return Restangular.withConfig(function(RestangularConfigurer) {

        RestangularConfigurer.setDefaultHttpFields({cache: true});
        RestangularConfigurer.setBaseUrl('http://localhost/data');
        //RestangularConfigurer.setBaseUrl('http://127.0.0.1:4000/data');
        RestangularConfigurer.setRequestSuffix('.json');
        RestangularConfigurer.setRestangularFields({
          id: "tid"
        });

      });

    });

  module.filter('htmlToPlaintext', function() {
    return function(text) {
      return String(text).replace(/<(?:.|\n)*?>/gm, '');
    }
  });



})();
