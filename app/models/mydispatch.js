// The contents of individual model .js files will be concatenated into dist/models.js

(function() {

// Protects views where angular is not loaded from errors
if ( typeof angular == 'undefined' ) {
	return;
};


var module = angular.module('MydispatchModel', []);

module.factory('myDispatch', function() {
  var db = window.openDatabase("Database", "1.0", "userprefs", -1);
  return {
    getSections: function (callback) {
      var self = this;
      var sections = [];
      db.transaction(function(tx) {
        tx.executeSql("SELECT tid FROM mydispatch", [], function(tx, results) {              
          var i = 0;
          var l = results.rows.length;
          for (; i < l; i++) {
            sections.push(results.rows.item(i).tid);
          }
          callback(sections);
        }, function(err) {
            callback(sections);
        });
      }, self.errorCB, self.successCB);
    },
    getDispatchSetting: function (callback) {
      var self = this;
      var item;
      db.transaction(function(tx) {
        tx.executeSql("SELECT setting FROM settings WHERE name = 'mydispatch'", [], function(tx, results) {              
          var len = results.rows.length;
          if (len > 0) {
            item = results.rows.item(0);
          }
          callback(item);
        }, function(err) {
            callback(item);
        });
      }, self.errorCB, self.successCB);
    },
    saveData: function (id, callback) {
      var self = this;
      db.transaction(
      function(tx) {
        tx.executeSql('INSERT OR IGNORE INTO mydispatch (tid) VALUES (?)', [id], function(tx, results) {
          callback(id);
        }, self.errorCB);        
      }, self.errorCB, self.successCB);
    },
    saveDispatchSetting: function (callback) {
      var self = this;
      db.transaction(
      function(tx) {
        var name = 'mydispatch';
        var setting = 1;
        tx.executeSql('INSERT OR REPLACE INTO settings (name, setting) VALUES (?,?)', [name, setting], function(tx, results) {
          callback(results);
        }, self.errorCB);        
      }, self.errorCB, self.successCB);
    },
    removeItem: function (id, callback) {
      var self = this;
      db.transaction(
      function(tx) {
        tx.executeSql('DELETE FROM mydispatch WHERE tid=?', [id], function(tx, results) {
        
        }, self.errorCB);
        callback(id);
      }, self.errorCB, self.successCB);
    },
    removeDispatchSetting: function (callback) {
      var self = this;
      db.transaction(
      function(tx) {
        tx.executeSql("UPDATE settings SET setting = 0 WHERE name = 'mydispatch'", [], function(tx, results) {
          callback(results);
        }, self.errorCB);        
      }, self.errorCB, self.successCB);
    },
    getItem: function (id, callback) {
      var self = this;
      var item;
      db.transaction(
      function(tx) {
        tx.executeSql('SELECT * FROM mydispatch WHERE tid=?', [id], function(tx, results) {
          var len = results.rows.length;
          if (len > 0) {
            item = results.rows.item(0);
          }
          callback(item);
        }, function(err) {
          callback(item);
        });
      }, self.errorCB, self.successCB);
    },
    errorCB: function (err) {
      //alert("Error processing SQL: "+err.code);
    },
    successCB: function () {
      //alert("success!");
    }

  }
});


})();
