# For an explanation of the steroids.config properties, see the guide at
# http://guides.appgyver.com/steroids/guides/project_configuration/config-application-coffee/

steroids.config.name = "AlaskaDispatch"

# -- Initial Location --
steroids.config.location = "http://localhost/index.html"

# -- Tab Bar --
#steroids.config.tabBar.enabled = true
#steroids.config.tabBar.tabs = [
#  {
#    title: "Font"
#    icon: "images/font-button-article@2x.png"
#    location: "http://localhost/index.html"
#  },
#  {
#    title: "share"
#    icon: "images/share-button-article@2x.png"
#    location: "http://www.google.com"
#  },
#  {
#    title: "save"
#    icon: "images/favorite-button-article@2x.png"
#    location: "http://www.google.com"
#  }
#]

#steroids.config.tabBar.tintColor = "#000000"
#steroids.config.tabBar.tabTitleColor = "#00aeef"
#steroids.config.tabBar.selectedTabTintColor = "#ffffff"
#steroids.config.tabBar.selectedTabBackgroundImage = "icons/pill@2x.png"

steroids.config.tabBar.backgroundImage = ""

# -- Navigation Bar --
steroids.config.navigationBar.tintColor = "#c92519"
steroids.config.navigationBar.titleColor = "#ffffff"
steroids.config.navigationBar.buttonTintColor = "#ffffff"
steroids.config.navigationBar.buttonTitleColor = "#ffffff"

steroids.config.navigationBar.portrait.backgroundImage = "images/portrait_navbar@2x.png"
# steroids.config.navigationBar.portrait.backgroundImage = ""

# -- Android Loading Screen
steroids.config.loadingScreen.tintColor = "#262626"

# -- iOS Status Bar --
steroids.config.statusBar.enabled = true
steroids.config.statusBar.style = "light"

# -- File Watcher --
# steroids.config.watch.exclude = ["www/my_excluded_file.js", "www/my_excluded_dir"]

# -- Pre- and Post-Make hooks --
# steroids.config.hooks.preMake.cmd = "echo"
# steroids.config.hooks.preMake.args = ["running yeoman"]
# steroids.config.hooks.postMake.cmd = "echo"
# steroids.config.hooks.postMake.args = ["cleaning up files"]

# -- Default Editor --
# steroids.config.editor.cmd = "subl"
# steroids.config.editor.args = ["."]
